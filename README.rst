SeqKit FASTQ Pair App
=====================

Version: 0.16.0-01

This GeneFlow app uses SeqKit to remove unpaired reads from FASTQ files. 
Inputs
------

1. input: Sequence FASTQ File - Sequence FASTQ file.

2. pair: Paired-End Sequence FASTQ File - A paired-end sequence FASTQ file.

Parameters
----------

1. threads: CPU Threads - Number of CPU threads to use. Default: 4.

2. output: Output Directory - The output directory for filtered FASTQ files. Default: output.
